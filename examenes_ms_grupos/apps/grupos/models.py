from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.crypto import get_random_string


class Grupo(models.Model):
    nombre = models.CharField(max_length=250, verbose_name='nombre del grupo')
    profesor = models.PositiveIntegerField()
    estudiantes = ArrayField(models.PositiveIntegerField())
    codigo = models.CharField(max_length=250, verbose_name='código del grupo (opcional)', help_text='Ejemplo: 7500001M', blank=True)
    numero = models.CharField(max_length=50, verbose_name='número del grupo (opcional)', blank=True)
    codigo_acceso = models.CharField(max_length=10)

    def __str__(self):
        return "%s %s %s"%(self.nombre, self.codigo, self.numero)

    class Meta:
        ordering = ['profesor', 'nombre']
        unique_together = ('nombre', 'codigo', 'numero')

    def save(self, *args, **kwargs):
        if not self.codigo_acceso:
            codigo_acceso = get_random_string(length=10, allowed_chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
            while Grupo.objects.filter(codigo_acceso=codigo_acceso).count()>0:
                codigo_acceso = get_random_string(length=10, allowed_chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
            self.codigo_acceso = codigo_acceso
        super(Grupo, self).save(*args, **kwargs)