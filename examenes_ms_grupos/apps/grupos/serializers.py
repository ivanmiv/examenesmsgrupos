from .models import *
from rest_framework import serializers


class CrearGrupoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Grupo
        fields = ('id', 'nombre', 'profesor', 'estudiantes', 'codigo', 'numero', 'url')

class ModificarGrupoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Grupo
        fields = ('id', 'nombre', 'estudiantes', 'codigo', 'numero', 'url')

class ListarGrupoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Grupo
        fields = ('id', 'nombre', 'estudiantes', 'codigo', 'numero', 'codigo_acceso', 'url')