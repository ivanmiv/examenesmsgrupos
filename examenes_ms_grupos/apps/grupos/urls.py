from django.urls import path
from .views import *

urlpatterns = [
    path('', ListarCrearGrupoView.as_view()),
    path('<int:pk>/', ObtenerModificarGrupoView.as_view(), name='grupo-detail'),
]
