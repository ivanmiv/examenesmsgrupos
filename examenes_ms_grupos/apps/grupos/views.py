from django_microservices.models import MicroService
from rest_framework import generics
from rest_framework.exceptions import PermissionDenied

from apps.grupos.models import Grupo
from apps.grupos.serializers import ModificarGrupoSerializer, CrearGrupoSerializer, ListarGrupoSerializer


class ListarCrearGrupoView(generics.ListCreateAPIView):
    queryset = Grupo.objects.all()
    serializer_class = CrearGrupoSerializer

    def get_serializer_class(self):
        serializer_class = self.serializer_class

        if self.request.method == 'GET':
            serializer_class = ListarGrupoSerializer

        return serializer_class

    def get_queryset(self):
        queryset = super().get_queryset()
        microservicio = MicroService.objects.get(name="api")
        usuario = self.request.META.get('HTTP_MICROSERVICE_USER_PK')
        if usuario is None or usuario == 'None':
            raise PermissionDenied({})

        try:
            respuesta = microservicio.remote_call(
                    "GET", api='api/usuarios/%s' % usuario, headers={'MICROSERVICE-USER-PK': str(usuario)}
            )
            if respuesta.status_code == 200:
                usuario = respuesta.json()
            else:
                usuario = {}
        except Exception as e:
            usuario = {}
        if usuario.get('rol') == 'Profesor':
            queryset = queryset.filter(profesor=int(usuario.get('id', 0)))
        elif usuario.get('rol') == 'Estudiante':
            queryset = queryset.filter(estudiantes__contains=[int(usuario.get('id', 0))])
        else:
            raise PermissionDenied({})
        return queryset


class ObtenerModificarGrupoView(generics.RetrieveUpdateAPIView):
    queryset = Grupo.objects.all()
    serializer_class = ModificarGrupoSerializer

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            profesor = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            profesor = None

        if profesor is None:
            raise PermissionDenied({})
        if profesor != instance.profesor:
            raise PermissionDenied({})
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            profesor = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            profesor = None

        if profesor is None:
            raise PermissionDenied({})
        if profesor != instance.profesor:
            raise PermissionDenied({})
        return self.update(request, *args, **kwargs)
